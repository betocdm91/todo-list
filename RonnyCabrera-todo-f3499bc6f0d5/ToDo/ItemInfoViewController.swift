//
//  ItemInfoViewController.swift
//  ToDo
//
//  Created by Carlos Osorio on 16/5/18.
//  Copyright © 2018 JorgeCarrillo. All rights reserved.
//

import UIKit

class ItemInfoViewController: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var locationLabel: UILabel!
    
    @IBOutlet weak var descriptionLabel: UILabel!
    
    var itemInfo: (itemManager: ItemManager, index: Int)?

    
    override func viewDidLoad() {
        super.viewDidLoad()

        titleLabel.text = itemInfo?.itemManager.toDoItem[(itemInfo?.index)!].title
        locationLabel.text = itemInfo?.itemManager.toDoItem[(itemInfo?.index)!].location
        descriptionLabel.text = itemInfo?.itemManager.toDoItem[(itemInfo?.index)!].description

    }
    
    @IBAction func checkButtonPressed(_ sender: Any) {
        itemInfo?.itemManager.checkItem(index: (itemInfo?.index)!)
        navigationController?.popViewController(animated: true)
    }
    
}
