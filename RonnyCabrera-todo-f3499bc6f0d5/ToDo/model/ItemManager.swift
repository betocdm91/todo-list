//
//  itemManager.swift
//  ToDo
//
//  Created by Ronny Cabrera on 9/5/18.
//  Copyright © 2018 RonnyCabrera. All rights reserved.
//

import Foundation

class ItemManager {
    var toDoItem:[Item] = []
    var doneItem:[Item] = []
    
    func checkItem(index:Int){
        let item = toDoItem.remove(at: index)
        doneItem += [item]
    }
    
    func unCheckItem(index:Int) {
        let item = doneItem.remove(at: index)
        toDoItem += [item]
    }
    
}
