//
//  item.swift
//  ToDo
//
//  Created by Ronny Cabrera on 9/5/18.
//  Copyright © 2018 RonnyCabrera. All rights reserved.
//

import Foundation

struct Item {
    let title: String
    let location: String
    let description: String
}
