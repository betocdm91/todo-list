//
//  itemTableViewCell.swift
//  ToDo
//
//  Created by Carlos Osorio on 22/5/18.
//  Copyright © 2018 JorgeCarrillo. All rights reserved.
//

import UIKit

class itemTableViewCell: UITableViewCell {

    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var locationLAbel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
