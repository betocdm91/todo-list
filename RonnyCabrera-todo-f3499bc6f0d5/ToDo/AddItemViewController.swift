//
//  AddItemViewController.swift
//  ToDo
//
//  Created by Ronny Cabrera on 9/5/18.
//  Copyright © 2018 RonnyCabrera. All rights reserved.
//

import UIKit

class AddItemViewController: UIViewController {

    @IBOutlet weak var titleTextFiled: UITextField!
    @IBOutlet weak var locationTextField: UITextField!
    @IBOutlet weak var descriptionTextFiled: UITextField!
    
    var itemManager:ItemManager?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func cancelButtonPressed(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func saveButtonPressed(_ sender: Any) {
        let itemTitle = titleTextFiled.text ?? ""
        let itemLocation = locationTextField.text ?? ""
        let itemDescription = descriptionTextFiled.text ?? ""
        
        /*if(itemTitle == ""){
            print( "Nuestro TextField Title está vacío")
        }else{
            print( "Nombre: \(titleTextFiled!)")
        }
        
        if(itemLocation == ""){
            print( "Nuestro TextField Location está vacío")
        }else{
            print( "Nombre: \(locationTextField!)")
        }
        
        if(itemDescription == ""){
            print( "Nuestro TextField Description está vacío")
        }else{
            print( "Nombre: \(descriptionTextFiled!)")
        }*/
        
        
        let item = Item(
            title: itemTitle,
            location:itemLocation,
            description:itemDescription
        )
        
        if item.title == ""{
            showAlert(title: "Error", message: "Title is required")
        }
        
        itemManager?.toDoItem += [item]
        
        navigationController?.popViewController(animated: true)
    }
    
    func showAlert(title:String, message:String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(okAction)
        
        present(alert, animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
