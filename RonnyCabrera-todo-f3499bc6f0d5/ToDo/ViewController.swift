//
//  ViewController.swift
//  ToDo
//
//  Created by RonnyCabrera on 8/5/18.
//  Copyright © 2018 RonnyCabrera. All rights reserved.
//

import UIKit


class ViewController: UIViewController, UITableViewDataSource, UITabBarDelegate, UITableViewDelegate{

    @IBOutlet weak var itemsTableView: UITableView!
    var itemManager = ItemManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    /*    let item1 = Item(title: "To Do 1", location: "Office", description: "Do Somehting")
        let item2 = Item(title: "To Do 2", location: "House", description: "Feed the Cat")
        let item3 = Item(title: "To Do 3", location: "Brasil", description: "Go to the beach")
        let item4 = Item(title: "To Do 4", location: "Narnia", description: "Eat")
        
        itemManager.toDoItem = [item1, item2, item3]
        itemManager.doneItem = [item4]
 */       // Do any additional setup after loading the view, typically from a nib.
 
 }
  
    override func viewWillAppear(_ animated: Bool) {
        itemsTableView.reloadData()
        
    }

    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?){
        if segue.identifier == "toAddItemSegue" {
            let destination = segue.destination as! AddItemViewController
            destination.itemManager = itemManager
        }
        
        if segue.identifier == "toIteminfoSegue" {
            let destination = segue.destination as! ItemInfoViewController
            let selectedRow = itemsTableView.indexPathsForSelectedRows![0]
            destination.itemInfo = (itemManager, selectedRow.row)
        }
        
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return itemManager.toDoItem.count
        }
        return itemManager.doneItem.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "itemCell") as! itemTableViewCell
        if indexPath.section == 0 {
            cell.titleLabel.text = itemManager.toDoItem[indexPath.row].title
            cell.locationLAbel.text = itemManager.toDoItem[indexPath.row].location

        }
        else {
            cell.titleLabel.text = itemManager.doneItem[indexPath.row].title
            cell.locationLAbel.text = ""
        }
        
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0{
            performSegue(withIdentifier: "toIteminfoSegue", sender: self)

        }

    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return section == 0 ? "To DO" : "Done"
    }
    
    func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? {
        return indexPath.section == 0 ? "Check" : "UnCheck"
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        if indexPath.section == 0 {
            itemManager.checkItem(index: indexPath.row)
        }
        
        else{
            itemManager.unCheckItem(index: indexPath.row)

        }
        
        itemsTableView.reloadData()
    }
}

